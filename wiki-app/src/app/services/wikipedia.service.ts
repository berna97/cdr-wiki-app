import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class WikipediaService {
  url = 'https://en.wikipedia.org/w/api.php';
  constructor(private http: HttpClient) { 

    // this.url = this.url + '?origin=*';
  }

  get getUrl() {
    return this.url;
  }

  search(term: string) {
    console.log(this.url);
    return this.http.get(this.url, {
      params : {
        origin: '*',
        action: 'query',
        list: 'search',
        srsearch: `${term}`,
        format: 'json'
      }
    });
  }



}
