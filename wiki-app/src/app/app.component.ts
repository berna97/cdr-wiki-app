import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { WikipediaService } from './services/wikipedia.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  pages = [];
  constructor(private ws: WikipediaService, private cdr: ChangeDetectorRef) { }

  searchedTerm(term: string) {
    this.ws.search(term).subscribe((response: any) => {
      this.pages = response.query.search;
      console.log(this.pages);
      this.cdr.markForCheck();
    });
  }
}
