import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-page-list-description',
  templateUrl: './page-list-description.component.html',
  styleUrls: ['./page-list-description.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageListDescriptionComponent implements OnInit {
  @Input() page: any;

  constructor() { }

  ngOnInit(): void {
  }

}
