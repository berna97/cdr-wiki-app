import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageListDescriptionComponent } from './page-list-description.component';

describe('PageListDescriptionComponent', () => {
  let component: PageListDescriptionComponent;
  let fixture: ComponentFixture<PageListDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageListDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageListDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
