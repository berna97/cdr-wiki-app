import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { PageListComponent } from './page-list/page-list.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { PageListTitleComponent } from './page-list-title/page-list-title.component';
import { PageListWordcountComponent } from './page-list-wordcount/page-list-wordcount.component';
import { PageListDescriptionComponent } from './page-list-description/page-list-description.component';

@NgModule({
  declarations: [
    AppComponent,
    PageListComponent,
    SearchBarComponent,
    PageListTitleComponent,
    PageListWordcountComponent,
    PageListDescriptionComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
