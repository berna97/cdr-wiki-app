import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-page-list',
  templateUrl: './page-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageListComponent implements OnInit {
  @Input() pages = [];
  constructor() { }

  ngOnInit(): void {
  }

}
