import { Component, OnInit, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchBarComponent implements OnInit {
  @Output() onsubmit = new EventEmitter<string>();
  term = '';
  constructor() { }

  ngOnInit(): void {

  }

  onFormSubmit(event) {
    event.preventDefault();
    this.onsubmit.emit(this.term);
    console.log('From the child component ', this.term);
  }

}
