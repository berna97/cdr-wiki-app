import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-page-list-title',
  templateUrl: './page-list-title.component.html',
  styleUrls: ['./page-list-title.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageListTitleComponent implements OnInit {
  @Input() page: any;

  constructor() { }

  ngOnInit(): void {
    console.log(this.page.title);
  }

}
