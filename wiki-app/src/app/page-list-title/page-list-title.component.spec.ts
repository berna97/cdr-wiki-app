import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageListTitleComponent } from './page-list-title.component';

describe('PageListTitleComponent', () => {
  let component: PageListTitleComponent;
  let fixture: ComponentFixture<PageListTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageListTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageListTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
