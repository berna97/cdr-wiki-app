import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageListWordcountComponent } from './page-list-wordcount.component';

describe('PageListWordcountComponent', () => {
  let component: PageListWordcountComponent;
  let fixture: ComponentFixture<PageListWordcountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageListWordcountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageListWordcountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
