import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-page-list-wordcount',
  templateUrl: './page-list-wordcount.component.html',
  styleUrls: ['./page-list-wordcount.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageListWordcountComponent implements OnInit {
  @Input() page: any;

  constructor() { }

  ngOnInit(): void {
  }

}
