# LAB | Wiki App



## Requirements

- Fork this repo
- Clone this repo

## Submission

- Upon completion, run the following commands:

  ```
  git add .
  git commit -m "done"
  git push origin master
  ```

- Create a Merge Request 

## Introduction

Wikipedia is always an option to start learning a new subject, Wouldn't it be great to have a wikipedia search engine just for us?. In this laboratory, you will build your own Wikipedia search bar to have direct access to all the knowledge packed inside Wikipedia

## Instructions


### Iteration 1: Create components for title, wordcount and snippter

Create three components `PageTitle`, `PageWordcount` and `PageSnippet`. And pass the page  data to the components

### Iteration 2: Change detection strategy

Finally, implement the OnPush change detection strategy on all commponents. The app should  be working as previous

Happy coding! :heart: